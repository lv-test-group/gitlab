// eslint-disable-next-line import/prefer-default-export
export const isEmpty = state => !state.rules || !state.rules.length;
